import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';


@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
})

export class MoviesComponent implements OnInit {
  studio = "";
  name = "no movie";
  displayedColumns: string[] = ['Id', 'Title', 'Studio', 'Weekend_Income', 'delete'];
  
  movies = [];
  studios = [];

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        this.studios = ['all'];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            this.movies.push(y);
            let stu = y['Studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['Studio']);
            }
          }
        )
      }
    )
  }

  filter() {
    let id = 1;
        this.db.list('/movies').snapshotChanges().subscribe(
          movies => {
            this.movies = [];
            movies.forEach(
              movie => {
                let y = movie.payload.toJSON();
                if (this.studio == 'all') {
                                this.movies.push(y);
                              }
                              else if (y['Studio'] == this.studio) {
                                y['Id'] = id;
                                id++;
                  this.movies.push(y);
                }
              }
            )
          }
        )
      }

  hideHim(mov) {
    let start, end = this.movies;
    let movid = mov.Id;

    if (this.movies.length == 1) {
      this.movies = [];
    }
    start = this.movies.slice(0,movid-1);
    end = this.movies.slice(movid, this.movies.length+1);
    this.movies = start.concat(end);
    this.name = mov.Title;

    for (let movie of this.movies) {
      if (movie.Id > movid) {
        movie.Id--;
      }
    }
  }
}