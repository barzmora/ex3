// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDK9I5sJ640huxDqvx17zKvvpbxC02JPXA",
    authDomain: "movies-378bb.firebaseapp.com",
    databaseURL: "https://movies-378bb.firebaseio.com",
    projectId: "movies-378bb",
    storageBucket: "movies-378bb.appspot.com",
    messagingSenderId: "106541081824"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
